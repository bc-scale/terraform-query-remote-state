#!/bin/bash

function usage() {
  echo "./get-state-output.sh <workspace-id> <user-token> <field-key>"
  exit 1
}

# Required
export WORKSPACE_ID=$1 ## or replace with value
export TOKEN=$2 ## or replace with value
export FIELD=$3

# Optional
export TERRAFORM_ORG_TOKEN="" ## IF ORG token is available (mostly for debugging) place here
export ORG_NAME="" ## INSERT ORG NAME (string) HERE
export ENTITLEMENTS="false"

export BASE_URL="https://app.terraform.io/api/v2"

######################## Pre-check ########################

if [ -z ${WORKSPACE_ID} ]; then echo "Workspace ID is a required field"; usage; fi
if [ -z ${TOKEN} ]; then echo "User Token is a required field"; usage; fi
if [ -z ${FIELD} ]; then echo "Field/key is a required field"; usage; fi

#########################  Main ###########################

function getValueFromOutputs() {

  local STATE=$(curl -s \
    --header "Authorization: Bearer ${TOKEN}" \
    --header "Content-Type: application/vnd.api+json" \
    --request GET \
    "${BASE_URL}/workspaces/${WORKSPACE_ID}/current-state-version?include=outputs")

  local VALUE=$(echo $STATE | jq -r ".included[].attributes | select(.name | contains(\"${FIELD}\")).value")

  echo $VALUE
}

export VAL=$(getValueFromOutputs)

echo -en "Get the Current State Version:\n\n${VAL}"






######################### DEBUG & EXTRAS ##################

if [ ${ENTITLEMENTS} == 'true' ]; then

  if [ -z ${ORG_NAME} ]; then echo "Organziation Name is a required for Entitlemens check"; usage; fi
  if [ -z ${TERRAFORM_ORG_TOKEN} ]; then echo "Organziation token is a required for Entitlemens check"; usage; fi

  echo "Show Organization Entitlements: "

  curl \
    --header "Authorization: Bearer ${TERRAFORM_ORG_TOKEN}" \
    --header "Content-Type: application/vnd.api+json" \
    ${BASE_URL}/organizations/${ORG_NAME}/entitlement-set | jq

  echo "Sow Details of Workspace: "

  curl \
    --header "Authorization: Bearer ${TOKEN}" \
    --header "Content-Type: application/vnd.api+json" \
    --request GET \
    "${BASE_URL}/workspaces/${WORKSPACE_ID}" | jq
fi
provider "google" {
  region = var.region
}

terraform {
    #   Cut-copy-pasted from the Workspace setup flow on Terraform Cloud
    #   backend "remote" {
    #     organization = "XXXXXXX"
    #     workspaces {
    #       name = "XXXX-name-of-the-workspace-XXXX"
    #     }
    #   }

    # NOTE: If existing remote state exists, will need to be transfered to Terraform Cloud
    #
    # EXISTING STATE EXAMPLE, cannot have both backends, comment Cloud backend
    #   while `terraform state pull` of existing remote state, then remove this section
    #
    #   backend "gcs" {
    #     bucket  = "XXXX-some-bucket-name-state-XXX"
    #     prefix  = "terraform/state"
    #   }
}

provider "random" {
}
